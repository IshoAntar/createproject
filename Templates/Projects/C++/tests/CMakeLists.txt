set(tests Add)

find_package(Catch2 REQUIRED)

include(Catch)

add_library(tests_main OBJECT tests_main.cpp)

foreach(test ${tests})
	add_executable(${test} test_${test}.cpp $<TARGET_OBJECTS:tests_main>)
	target_link_libraries(${test} Catch2::Catch2)
	catch_discover_tests(${test})
endforeach(test ${tests})
