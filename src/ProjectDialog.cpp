/*
 * Copyright 2021  Isho Antar <IshoAntar@protonmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "ProjectDialog.h"
#include "ui_ProjectDialog.h"

#include "Project.h"

#include <QString>

ProjectDialog::ProjectDialog()
	: m_ui(new Ui::ProjectDialog)
{
	m_ui->setupUi(this);

	m_ui->commitFiles->setVisible(false);
	m_ui->commitMessage->setVisible(false);

	QObject::connect(m_ui->versionControl, &QComboBox::currentTextChanged, [this](const QString& text) {
		if (text != "None")
		{
			m_ui->commitFiles->setVisible(true);
			m_ui->commitMessage->setVisible(true);
		}
	});
	QObject::connect(m_ui->commitFiles, &QCheckBox::stateChanged, [this](int state) {
		if (state == Qt::CheckState::Checked)
		{
			m_ui->commitMessage->setEnabled(true);
		}
		else
		{
			m_ui->commitMessage->setEnabled(false);
		}
	});
}

ProjectDialog::~ProjectDialog()
{
	delete m_ui;
}

void ProjectDialog::SetOutputPath(const QString& path)
{
	m_ui->outputPath->setText(path);
}

void ProjectDialog::SetTemplatePath(const QString& path)
{
	m_ui->templatePath->setText(path);
}

QString ProjectDialog::ProjectName() const
{
	return m_ui->projectName->text();
}

QString ProjectDialog::ProjectVersion() const
{
	return m_ui->projectVersion->text();
}

QString ProjectDialog::ProjectType() const
{
	return m_ui->projectType->currentText();
}

QString ProjectDialog::ProjectVcs() const
{
	return m_ui->versionControl->currentText();
}

bool ProjectDialog::CommitFiles() const
{
	return m_ui->commitFiles->isChecked();
}

QString ProjectDialog::CommitMessage() const
{
	return m_ui->commitMessage->toPlainText();
}

Project* ProjectDialog::execute()
{
	if (exec() == QDialog::DialogCode::Rejected)
	{
		return nullptr;
	}

	return CreateNewProject(ProjectName().toStdString(),
							ProjectVersion().toStdString(),
							m_ui->templatePath->text().toStdString(),
							m_ui->outputPath->text().toStdString());
}
