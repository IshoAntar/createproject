/*
 * Copyright 2021  Isho Antar <IshoAntar@protonmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "CMakeProject.h"

#include <QMessageBox>

#include <algorithm>
#include <filesystem>
#include <fmt/format.h>

#include <qmessagebox.h>
#include <utility>
#include <fstream>

CMakeProject::CMakeProject(std::string name,
						   std::string version,
						   std::filesystem::path templatePath,
						   std::filesystem::path outputPath)
	: Project(std::move(name), std::move(version), std::move(templatePath), std::move(outputPath))
{
}

bool CMakeProject::OnCreated(const std::filesystem::path& outputPath)
{
	using std::filesystem::copy;
	using std::filesystem::copy_options;
	using std::filesystem::create_directories;
	using std::filesystem::exists;
	using std::filesystem::is_empty;
	using std::filesystem::path;

	// Create public include directory
	auto ec = std::error_code();
	const auto publicIncludeDir = outputPath / "include" / name();
	create_directories(publicIncludeDir, ec);

	if (ec)
	{
		auto mb = QMessageBox();
		mb.setIcon(QMessageBox::Icon::Warning);
		mb.setInformativeText(
		  fmt::format("Failed to create public include directory '{}'", publicIncludeDir.string()).c_str());
		mb.setDetailedText(ec.message().c_str());
		mb.exec();

		// Non-fatal error, no need to return!
	}

	return ProcessCMakeLists();
}

bool CMakeProject::ProcessCMakeLists()
{
	// TODO Error checking

	const auto cmakelistsPath = outputPath() / name() / "CMakeLists.txt";

	auto cmakelistsIn = std::ifstream(cmakelistsPath);
	auto cmakelistsOut = std::ofstream(cmakelistsPath.string() + ".new");
	auto line = std::string();

	const std::pair<std::string, std::string> cmakeVars[] = { { "$PROJECT_NAME", name() },
															  { "$PROJECT_VERSION", version() } };

	while (std::getline(cmakelistsIn, line))
	{
		for (const auto& var : cmakeVars)
		{
			auto varPos = 0UL;
			while ((varPos = line.find(var.first)) != std::string::npos)
			{
				line.replace(varPos, var.first.length(), var.second);
			}
		}

		cmakelistsOut << line << '\n';
	}

	std::filesystem::remove(cmakelistsPath);
	std::filesystem::rename(cmakelistsPath.string() + ".new", cmakelistsPath);

	return true;
}
