/*
 * Copyright 2021  Isho Antar <IshoAntar@protonmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Project.h"

#include "Impl/GenericProject.h"
#include "Impl/CMakeProject.h"

#include <QMessageBox>

#include <fmt/format.h>
#include <fmt/compile.h>

static Project::Type RetrieveProjectType(const std::filesystem::path& path)
{
	using Type = Project::Type;

	if (std::filesystem::exists(path / "CMakeLists.txt"))
	{
		return Type::CMake;
	}

	return Project::Type::Generic;
}

Project::Project(std::string name,
				 std::string version,
				 std::filesystem::path templatePath,
				 std::filesystem::path outputPath)
	: m_Name(std::move(name))
	, m_Version(std::move(version))
	, m_TemplatePath(std::move(templatePath))
	, m_OutputPath(std::move(outputPath))
{
}

bool Project::create()
{
	using std::filesystem::copy_options;

	const auto projectPath = m_OutputPath / m_Name;

	auto ec = std::error_code();

	if (!exists(projectPath))
	{
		if (!create_directories(projectPath, ec))
		{
			auto mb = QMessageBox();
			mb.setIcon(QMessageBox::Icon::Critical);
			mb.setInformativeText("Failed to create output directory");
			mb.setDetailedText(ec.message().c_str());
			mb.exec();

			return false;
		}
	}
	else if (is_empty(projectPath))
	{
		auto mb = QMessageBox();
		mb.setIcon(QMessageBox::Icon::Question);
		mb.setText(fmt::format("The directory '{}' exists but it is empty.", projectPath.string()).c_str());
		mb.setInformativeText("Proceed anyway?");
		mb.setStandardButtons(QMessageBox::Button::Ok | QMessageBox::Button::Cancel);

		if (mb.exec() == QMessageBox::Cancel)
		{
			return false;
		}
	}
	else
	{
		auto mb = QMessageBox();
		mb.setIcon(QMessageBox::Icon::Critical);
		mb.setText(fmt::format("The directory '{}' exists and is not empty.", projectPath.string()).c_str());
		mb.exec();

		return false;
	}

	copy(templatePath(), projectPath, copy_options::recursive, ec);

	if (ec)
	{
		auto mb = QMessageBox();
		mb.setIcon(QMessageBox::Icon::Critical);
		mb.setInformativeText(
		  fmt::format("Failed to copy template directory '{}' to '{}'", templatePath().string(), projectPath.string())
			.c_str());
		mb.setDetailedText(ec.message().c_str());
		mb.exec();

		return false;
	}

	return OnCreated(projectPath);
}

Project* CreateNewProject(const std::string& name,
						  const std::string& version,
						  const std::filesystem::path& templatePath,
						  const std::filesystem::path& outputPath)
{
	switch (RetrieveProjectType(templatePath))
	{
	case Project::Type::Generic: return new GenericProject(name, version, templatePath, outputPath);
	case Project::Type::CMake: return new CMakeProject(name, version, templatePath, outputPath);
	}

	return nullptr;
}
