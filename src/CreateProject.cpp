/*
 * Copyright 2021  Isho Antar <IshoAntar@protonmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "CreateProject.h"

#include "ProjectDialog.h"
#include "Project.h"

#include <KPluginFactory>
#include <KPluginLoader>
#include <KCoreAddons/KAboutData>
#include <KWidgetsAddons/KActionMenu>

#include <qglobal.h>
#include <QDebug>
#include <QtCore/QProcess>
#include <QDialog>
#include <QMessageBox>
#include <QProcess>

#include <fmt/os.h>

#include <fstream>

static void InitRepo(Vcs vcs, const std::filesystem::path& path, bool commitFiles, const std::string& commitMessage)
{
	switch (vcs)
	{
	case Vcs::None: break;
	case Vcs::Git:
		// TODO
		//   1. make sure git is installed
		//   2. Get the executable from `PATH`

		auto* proc = new QProcess();

		QObject::connect(proc, &QProcess::errorOccurred, [&path](QProcess::ProcessError error) {
			using Error = QProcess::ProcessError;

			QMessageBox mb;
			mb.setIcon(QMessageBox::Icon::Critical);
			mb.setInformativeText(fmt::format("Error initializing repository: {}", path.string()).c_str());

			switch (error)
			{
			case Error::FailedToStart: mb.setText("The process failed to start"); break;
			case Error::Crashed: mb.setText("The process crashed"); break;
			case Error::Timedout: mb.setText("The process has timeout"); break;
			case Error::UnknownError: mb.setText("Unknown error"); break;
			}

			mb.exec();
		});

		QObject::connect(proc,
						 QOverload<int, QProcess::ExitStatus>::of(&QProcess::finished),
						 [&path, proc](int /* exitCode */, QProcess::ExitStatus status) {
							 if (status == QProcess::ExitStatus::CrashExit)
							 {
								 QMessageBox mb;
								 mb.setIcon(QMessageBox::Icon::Critical);
								 mb.setInformativeText("Git error");
								 mb.setText(QString("Failed to initialze git repository: ") + path.c_str());
								 mb.setDetailedText(proc->readAllStandardError());
								 mb.exec();
							 }
						 });

		proc->start("/usr/bin/git", { "init", path.c_str() });
		proc->waitForFinished();

		if (commitFiles)
		{
			proc->start("/usr/bin/git", { "-C", path.c_str(), "add", "." });
			proc->waitForFinished();

			proc->start("/usr/bin/git", { "-C", path.c_str(), "commit", "-m", commitMessage.c_str() });
			proc->waitForFinished();
		}

		break;
	}
}

static void Create(const QString& templatePath, const QString& outputPath)
{
	auto dialog = ProjectDialog();

	dialog.SetOutputPath(outputPath);
	dialog.SetTemplatePath(templatePath);

	auto* project = dialog.execute();

	if (project == nullptr)
	{
		return;
	}

	if (!project->create())
	{
		return;
	}

	auto vcs = dialog.ProjectVcs() == "Git" ? Vcs::Git : Vcs::None;
	auto repoPath = std::filesystem::path(outputPath.toStdString()) / dialog.ProjectName().toStdString();
	auto commitFiles = dialog.CommitFiles();
	auto commitMessage = dialog.CommitMessage();

	InitRepo(vcs, repoPath, commitFiles, commitMessage.toStdString());
}

CreateProject::CreateProject(QObject* parent, const QVariantList& args)
	: KAbstractFileItemActionPlugin(parent)
{
	Q_UNUSED(args)

	auto username = qEnvironmentVariable("USER");
	if (username.isEmpty())
	{
		return;
	}

	auto userHome = std::filesystem::path(std::string("/home/") + username.toStdString());
	m_TemplatesDir = std::filesystem::path(userHome / "Templates/Projects");
	if (!std::filesystem::exists(m_TemplatesDir))
	{
		return;
	}

	EnumerateTemplates();
}

void CreateProject::EnumerateTemplates()
{
	for (const auto& d : std::filesystem::directory_iterator(m_TemplatesDir))
	{
		m_ProjectTemplates.push_back(d.path());
	}
}

QList<QAction*> CreateProject::actions(const KFileItemListProperties& fileItemInfos, QWidget* parentWidget)
{
	Q_UNUSED(parentWidget)

	if (fileItemInfos.items().size() > 1 || m_ProjectTemplates.empty())
	{
		return {};
	}

	const auto& targetItem = fileItemInfos.items().at(0);
	if (!(targetItem.url().isLocalFile() && targetItem.isWritable()))
	{
		return {};
	}

	auto* menuAction = new KActionMenu(this);
	menuAction->setIcon(QIcon::fromTheme("kdevelop"));
	menuAction->setText("Create Project");

	const auto& targetDir = targetItem.url().path();

	for (const auto& templ : m_ProjectTemplates)
	{
		auto* action = new QAction(QIcon::fromTheme("planetkde"), templ.filename().c_str(), this);
		connect(action, &QAction::triggered, this, [templ, targetDir] {
			Create(templ.c_str(), targetDir);
		});
		menuAction->addAction(action);
	}

	return { menuAction };
}

K_PLUGIN_CLASS_WITH_JSON(CreateProject, "CreateProject.json")

#include "CreateProject.moc"
