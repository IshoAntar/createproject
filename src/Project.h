/*
 * Copyright 2021  Isho Antar <IshoAntar@protonmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PROJECT_H
#define PROJECT_H

#include <string>
#include <filesystem>

class Project
{
public:
	enum class Type
	{
		Generic, /**< Project templates that don't require special handling, copy-pasted as-is */
		CMake
	};

public:
	Project(std::string name,
			std::string version,
			std::filesystem::path templatePath,
			std::filesystem::path outputPath);

	virtual ~Project() = default;

	[[nodiscard]] bool create();

	[[nodiscard]] const std::string& name() const { return m_Name; }
	[[nodiscard]] const std::string& version() const { return m_Version; }
	[[nodiscard]] const std::filesystem::path& templatePath() const { return m_TemplatePath; }
	[[nodiscard]] const std::filesystem::path& outputPath() const { return m_OutputPath; }

protected:
	/**
	 * @brief Called after the template is copied successfully to the output path.
	 */
	[[nodiscard]] virtual bool OnCreated(const std::filesystem::path& outputPath) = 0;

private:
	std::string m_Name;
	std::string m_Version;
	std::filesystem::path m_TemplatePath;
	std::filesystem::path m_OutputPath;
};

[[nodiscard]] Project* CreateNewProject(const std::string& name,
										const std::string& version,
										const std::filesystem::path& templatePath,
										const std::filesystem::path& outputPath);

#endif // PROJECT_H
